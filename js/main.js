$(document).ready(() => {
    console.log(window.location.hostname)
    switch (window.location.hostname) {
        case 'wwww.zara.com':
            crawlZara();
            break;
        case 'www.assos.com':
            crawlAssos();
            break;
        case 'www.sezane.com':
            crawlSezane();
            break;
        case 'www.maisonchateaurouge.com':
            crawlMaisonChateuRouge();
            break;
    }
})