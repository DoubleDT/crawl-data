function crawlSezane() {
    const title = $('.c-product__title').text().trim();
    const price = $('.c-product__price:first').text().trim();
    let description = '';
    $('ul.c-list li').each(function() {
        description += `${$(this).text()}\n`;
    });
    
    const images = $('.c-zoom > img.u-w-100').map(function () {
        let link = $(this).attr('src')
        return link;
    });
    console.log({
        title, price, description, images
    })
}