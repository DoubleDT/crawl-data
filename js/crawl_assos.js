function crawlAssos() {
    const title = $('.product-details-full-content-header-title').text();
    const price = $('span.facets-item-cell-grid-price-text').text().replace('USD &lrm', '');
    const description = $('span.first-half-desc-text').text();
    const images = $('img.zoomImg').map(function () {
        let link = $(this).attr('src')
        return link;
    });
    console.log({
        title, price, description, images
    })
}