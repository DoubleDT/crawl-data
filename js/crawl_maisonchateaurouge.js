function crawlMaisonChateuRouge() {
    const title = $('.ProductItem-details-title').text();
    const price = $('span.sqs-money-native:first').text();
    let description = ''
    description += `${$('.ProductItem-details-excerpt > p').text()} \n`;
    $('.ProductItem-details-excerpt ul li').each(function() {
        description += `${$(this).text()}\n`;
    });
    const images = $('img.ProductItem-gallery-slides-item-image').map(function () {
        let link = $(this).attr('src')
        return link;
    });
    console.log({
        title, price, description, images
    })
}