function crawlZara() {
    const title = $('.product-name').text();
    const price = $('span.main-price').text();
    const description = $('#description > p').text();
    const images = $('a.main-image').map(function () {
        let link = $(this).find('img').attr('src')
        return link;
    });
    const color = $('p.product-color span._colorName').text();
    console.log({
        title, price, description, images, color
    })
}