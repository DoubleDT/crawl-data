$(document).ready(() => {
    console.log('ok')
    function injectTheScript() {
        console.log('ok')
        chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
            // query the active tab, which will be only one tab
            //and inject the script in it
            chrome.tabs.executeScript(tabs[0].id, { file: "jquery.min.js" });
            chrome.tabs.executeScript(tabs[0].id, { file: "./js/crawl_zara.js" });
            chrome.tabs.executeScript(tabs[0].id, { file: "./js/crawl_assos.js" });
            chrome.tabs.executeScript(tabs[0].id, { file: "./js/crawl_sezane.js" });
            chrome.tabs.executeScript(tabs[0].id, { file: "./js/crawl_maisonchateaurouge.js" });
            chrome.tabs.executeScript(tabs[0].id, { file: "./js/main.js" });
        });
    }

    document.getElementById('crawl').addEventListener('click', injectTheScript);
})